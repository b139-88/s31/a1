const Task = require('../models/Task');

module.exports.createTask = (reqBody) => {
    let newTask = new Task({
        name: reqBody.name
    });

    return newTask.save().then((result) => result);
};

module.exports.getAllTasks = () => {
    return Task.find()
    .then((task) => task);
};

module.exports.getTask = (params) => {
    return Task.findById(params.id)
    .then((task) => task);
};

module.exports.updateTask = (reqbody) => {
    return Task.findOneAndUpdate(reqbody.name, { 
        status: reqbody.status 
    }, { 
        new: true 
    }).then((task) => task);
};

module.exports.deleteTask = (reqbody) => {
    return Task.deleteOne({
        name: reqbody.name
    })
    .then((task) => task);
};