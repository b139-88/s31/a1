
//import express module
const express = require("express");

//import mongoose module
const mongoose = require("mongoose");

//express server
const app = express();

//local port and available port after project hosted in heroku
const PORT = process.env.PORT || 4000;



//after exporting router module, import it to index.js file
const userRoutes = require("./routes/userRoutes");
const taskRoutes = require("./routes/taskRoutes");



//mongodb connection
	//connection string from mongodb atlas
	//object: useNewUrlParser & useUnifiedTopology
mongoose.connect('mongodb+srv://raymond:Mdba9189174@cluster0.ugfsr.mongodb.net/TaskDB?ssl=true&authSource=admin', {useNewUrlParser: true, useUnifiedTopology: true});

//mongodb notification
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log(`Connected to Database`));


/*Middlewares*/

//helps express to understand json payloads
app.use(express.json());

//helps express to understand json payloads
//json payloads come from forms and tables
app.use(express.urlencoded({extended:true}))


/*Routes
	Root url:
		http://localhost:4000/api/users
*/

//middleware that passes all requests to userRoutes module
app.use("/api/users", userRoutes);
app.use("/api/tasks", taskRoutes);


app.listen(PORT, () => console.log(`Server running at port ${PORT}`))

