const express = require('express');
const router = express.Router();

const taskController = require('../controllers/taskControllers');

//Following REST API principles

router.get('/', (req, res) => {
    taskController.getAllTasks().then((result) => res.send(result));
});

router.get('/:id', (req, res) => {
    taskController.getTask(req.params).then((result) => res.send(result));
});

router.post('/', (req, res) => {
    taskController.createTask(req.body).then((result) => res.send(result));
});

router.put('/', (req, res) => {
    taskController.updateTask(req.body).then((result) => res.send(result));
});

router.delete('/', (req, res) => {
    taskController.deleteTask(req.body).then((result) => res.send(result));
});

module.exports = router;