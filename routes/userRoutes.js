
//object that handles client requests and send back responses
//property of express module
	//Router();

const express = require("express");
const router = express.Router();

//in order to use controllers module properties, import controllers module
const userController = require("./../controllers/userControllers");

//Retrieving array of documents from database using "GET" method and find() model method
router.get("/", (req, res) => {
	//check the request if there's data to be used
	// console.log(req);

	//invoke the function from controllers module
	userController.getAllUsers().then( result => {
		res.send(result);
	});
});


//Add a user in the database using "POST" http method and save() method
router.post("/add-user", (req, res) => {
	//check the request if there's data to be used
	// console.log(req.body);

	//invoke the function from controllers module
	userController.register(req.body).then((result) => res.send(result));
});

router.put("/update-user", (req, res) => {
	userController.updateUser(req.body.email).then((result) => res.send(result));
});


//in order for the routes to be use in other modules, we need to export it first
module.exports = router;